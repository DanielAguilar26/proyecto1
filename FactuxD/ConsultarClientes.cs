﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class ConsultarClientes : Consultas
    {
        public ConsultarClientes()
        {
            InitializeComponent();
        }

        private void ConsultarClientes_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGV("Cliente").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtBuscar.Text.Trim())== false) 
            {
                try {

                    DataSet DS;
                    string cmd = "SELECT * FROM Cliente WHERE Nom_cli LIKE ('%" + txtBuscar.Text.Trim() + "%') ";

                    DS = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = DS.Tables[0];
                }catch(Exception error) {
                    MessageBox.Show("¡ha ocurrido un error!","Error"+error.Message);
                }
            }
        }
    }
}
