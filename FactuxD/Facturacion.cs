﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class Facturacion : Procesos
    {
        public Facturacion()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Facturacion_Load(object sender, EventArgs e)
        {
           
            string cmd = "SELECT * FROM Usuario WHERE id_usuario="+VentanaLogin.codigo;
            DataSet DS = Utilidades.Ejecutar(cmd);
            txtAtiende.Text = DS.Tables[0].Rows[0]["Nom_usu"].ToString().Trim();
         
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtCodigo.Text.Trim())==false) {

                try { 

                    string cmd = string.Format("SELECT Nom_cli FROM Cliente WHERE id_clientes='{0}'",txtCodigo.Text.Trim());
                    DataSet DS = Utilidades.Ejecutar(cmd);
                    txtCliente.Text = DS.Tables[0].Rows[0]["Nom_cli"].ToString().Trim();

                    txtCodigo.Focus();

                }catch(Exception error) {
                    MessageBox.Show("Ha ocurrido un error: ","Error"+error.Message);
                }
            }
        }

        public static int cont_fila = 0;
        public static double total;

        private void btnColocar_Click(object sender, EventArgs e)
        {
            if (Utilidades.ValidarFormulario(this,errorProvider1)==false)
             {
                bool existe = false;
                int num_fila = 0;

                if(cont_fila == 0) {
                    dataGridView1.Rows.Add(txtCodigoDG.Text,txtDescripcion.Text, txtPrecio.Text,txtCantidad.Text);
                    double importe = Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[2].Value) *
                    Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[3].Value);
                    dataGridView1.Rows[cont_fila].Cells[4].Value = importe;

                    cont_fila++;
                }else {

                    foreach(DataGridViewRow Fila in dataGridView1.Rows)
                    {
                        if(Fila.Cells[0].Value.ToString() == txtCodigoDG.Text)
                        {
                            existe = true;
                            num_fila = Fila.Index;
                        }
                    }

                    if(existe == true )
                    {
                        dataGridView1.Rows[num_fila].Cells[3].Value = (Convert.ToDouble(txtCantidad.Text) + Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[3].Value)).ToString();
                        double importe = Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value) *
                        Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[3].Value);

                        dataGridView1.Rows[num_fila].Cells[4].Value = importe;
                    }else {
                        dataGridView1.Rows.Add(txtCodigoDG.Text, txtDescripcion.Text, txtPrecio.Text, txtCantidad.Text);
                        double importe = Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[2].Value) *
                        Convert.ToDouble(dataGridView1.Rows[cont_fila].Cells[3].Value);
                        dataGridView1.Rows[cont_fila].Cells[4].Value = importe;

                        cont_fila++;
                    }
                }

                total = 0;

                foreach (DataGridViewRow Fila in dataGridView1.Rows)
                {
                    total += Convert.ToDouble(Fila.Cells[4].Value);
                }

                lblTotalF.Text = "MXN$ " + total.ToString();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(cont_fila>0)
            {
                total = total - (Convert.ToDouble(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value));
                lblTotalF.Text = "MXN$ " + total.ToString();

                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
                cont_fila--;
            }
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            ConsultarClientes CC = new ConsultarClientes();
            CC.ShowDialog();

            if(CC.DialogResult == DialogResult.OK)
            {
                txtCodigo.Text = CC.dataGridView1.Rows[CC.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtCliente.Text = CC.dataGridView1.Rows[CC.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();

                txtCodigo.Focus();
            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            ConsultarProductos CP = new ConsultarProductos();
            CP.ShowDialog();

            if(CP.DialogResult == DialogResult.OK)
            {
                txtCodigoDG.Text = CP.dataGridView1.Rows[CP.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtDescripcion.Text = CP.dataGridView1.Rows[CP.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtPrecio.Text = CP.dataGridView1.Rows[CP.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();

                txtCantidad.Focus();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Nuevo();
        }

        public override void Nuevo()
        {
            txtCodigo.Text = "";
            txtCliente.Text = "";
            txtCodigoDG.Text = "";
            txtDescripcion.Text = "";
            txtPrecio.Text = "";
            txtCantidad.Text = "";
            lblTotalF.Text = "MXN$ 0";
            dataGridView1.Rows.Clear();
            cont_fila = 0;
            total = 0;

            txtCodigo.Focus();
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void txtCodigoDG_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }
    }
}
