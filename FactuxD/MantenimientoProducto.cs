﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class MantenimientoProducto : Mantenimiento
    {
        public MantenimientoProducto()
        {
            InitializeComponent();
        }

        private void MantenimientoProducto_Load(object sender, EventArgs e)
        {

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {

        }

        public override Boolean Guardar()
        {

            if (Utilidades.ValidarFormulario(this, errorProvider1) == false) 
            {
                try
                {
                    string cmd = string.Format("Exec ActualizaArticulos '{0}','{1}','{2}'", txtIdProducto.Text.Trim(), txtDescripcion.Text.Trim(), txtPrecio.Text.Trim());
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se ha guardado correctamente", "Insertar Producto");
                    return true;

                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                    return false;
                }
            }else {
                return false;
            }
           
        }

        public override void Eliminar()
        {
            try {
                string cmd = string.Format("EXEC EliminarArticulos '{0}'",txtIdProducto.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eleiminado correctamente","Eliminar Producto");
                

            }catch(Exception error){
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void txtIdProducto_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }
    }
}
