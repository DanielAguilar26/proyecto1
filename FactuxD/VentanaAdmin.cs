﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class VentanaAdmin : FormBase
    {
        public VentanaAdmin()
        {
            InitializeComponent();
            CenterToScreen();

        }

        private void VentanaAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void VentanaAdmin_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuario WHERE id_usuario="+VentanaLogin.codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblAdministrador.Text = DS.Tables[0].Rows[0]["Nom_usu"].ToString();
            lblUser.Text = DS.Tables[0].Rows[0]["account"].ToString();
            lblCod.Text = DS.Tables[0].Rows[0]["id_usuario"].ToString();

            string url = DS.Tables[0].Rows[0]["foto"].ToString();

            pictureBox1.Image = Image.FromFile(url);
        }

        private void btnPrincipal_Click(object sender, EventArgs e)
        {
            ContenedorPrincipal conP = new ContenedorPrincipal();
            this.Hide();
            conP.Show();
        }
    }
}
