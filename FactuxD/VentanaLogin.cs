﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;


namespace FactuxD
{
    public partial class VentanaLogin : FormBase
    {
        public static String codigo = "";

        public VentanaLogin()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("SELECT * FROM Usuario WHERE account='{0}' AND password='{1}'", txtAccount.Text.Trim(), txtPass.Text.Trim());
                DataSet ds = Utilidades.Ejecutar(cmd);

                codigo = ds.Tables[0].Rows[0]["id_usuario"].ToString().Trim();
                string cuenta = ds.Tables[0].Rows[0]["account"].ToString().Trim();
                string contra = ds.Tables[0].Rows[0]["password"].ToString().Trim();

                if (cuenta == txtAccount.Text.Trim() && contra == txtPass.Text.Trim())
                {
                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["status_admin"]) == true)
                    {
                        VentanaAdmin va = new VentanaAdmin();
                        this.Hide();
                        va.Show();

                    }
                    else if(Convert.ToBoolean(ds.Tables[0].Rows[0]["status_admin"]) == false)
                    { 
                        VentanaUser vu = new VentanaUser();
                        this.Hide();
                        vu.Show();
                    }
                }



            }
            catch (Exception error)
            {
                MessageBox.Show("¡Usuario o constraseña incorrecta!" + error.Message);
            }
        }

        private void VentanaLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
